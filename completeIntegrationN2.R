#!/usr/bin/env Rscript

## Creates the missing files for the integration level 2 from a csv file with the factor portfolio returns called e.g. myfile.csv located in portdir and creates a directory called fb_myfile_core_default
## Example of usage:
# cd ~/work/financial-utilities
# ./completeIntegrationN2.R --portdir=~/work/betablender-cl --universefile=myfactorsreturnsfile.csv --factornames="Barclays US Momentum Equity hedged Index,Barclays US Low Volatility Equity hedged Index,Barclays US Value Equity Market Hedged Index ER,Barclays US Quality Equity Market Hedged ER Index,Barclays Capital US Tracker ER Index,Libor 3 Months,Libor 3 Months"

if (exists("runargs")) {
  ## e.g. run from debugger with
  argv <- strsplit(runargs, ' ')[[1]]
  } else {
    argv <- commandArgs(trailingOnly = TRUE)
  }

## Take warnings seriously
options(warn=2)
## load standard code (which we need for listDay)
mydir <- getwd()
setwd(file.path('~','work','betablender-cl'))
## we are willing to forgive startup messages, typically to do with library()
suppressWarnings(source("rscripts/multibeta.R"))
setwd(mydir)

## Process the command line options:
suppressPackageStartupMessages(library("optparse"))
option_list <- list(
  make_option("--portdir", default='~/work/betablender-cl',
    help=paste0("directory to put files [default %default]")),
  make_option("--universefile", default=NULL,
    help=paste0("csv file with DATE, and total return series of the factor portfolios (matrix). [default %default]")),
  make_option("--size", default="core",
    help=paste0("size tag for the directory name [default %default]")),
  make_option("--weight", default="default",
    help=paste0("Factor Blender weightings_settings-*.sh tag for the directory name [default %default]")),
  make_option("--factornames", default=NULL,
    help="A comma separated list of factor names. If unspecified, will use the column names from the universefile."),
  make_option(c("--tickers"), default=NULL,
    help="A comma separated list of tickers. If unspecified, will use the factornames."),
  make_option("--onlyretfiles", action="store_true", default=FALSE,
    help="Only create the return files from the universefile (do not create holdings, rebaltrades and rebalDates files). [default %default]"),
  make_option("--notretfiles", action="store_true", default=FALSE,
    help="Only create holdings, rebaltrades and rebalDates files from the return files (do not create the return files). [default %default]")
)
# get command line options, if help option encountered print help and exit,
# otherwise if options not found on command line then set defaults,
opt <- parse_args(OptionParser(option_list=option_list), argv)
portdir <- opt$portdir
universefile <- opt$universefile
size <- opt$size
weight <- opt$weight
if (!is.null(opt$factornames)) {
  factornames <- strsplit(opt$factornames, ",")[[1]]
}
if (!is.null(opt$tickers)) {
  tickers <- strsplit(opt$tickers, ",")[[1]]
}
onlyretfiles <- opt$onlyretfiles
notretfiles <- opt$notretfiles

## create return files from the universefile (simple chopping)
if (!notretfiles) {
    Factors <- fread(file.path(portdir,universefile))
    cols_with_names <- names(Factors)[names(Factors) != ""]
    Factors <- Factors[,cols_with_names,with=FALSE]
    Factors <- Factors[complete.cases(Factors),]

    fnames <- setdiff(names(Factors),"DATE")
    universefile <- gsub(".csv","",universefile)
    universefile <- paste("fb",universefile,size,weight,sep="_")

    for (fname in fnames) {
        if (fname == "cwall") {
            prefix <- "b-"
        } else prefix <- "f-"
        fret <- Factors[,c("DATE",fname),with=FALSE]
        setnames(fret,fname,"RT")
        fret[,RP:=RT]
        fret[,RN:=RT]
        fret <- fret[,list(DATE,RP,RT,RN)]
        dir.create(file.path(portdir,universefile), showWarnings = FALSE)
        ffilename <- paste0(prefix,fname,"-returns.csv")
        write.csv(fret,file=file.path(portdir,universefile,ffilename), row.names=FALSE)
        if (fname == "cwall" && !is.element("neutral",fnames) ) {
            write.csv(fret,file=file.path(portdir,universefile,"f-neutral-returns.csv"), row.names=FALSE)
        }
    }
    retDir <- file.path(portdir,universefile)
} else {
    retDir <- portdir
    fnames <- gsub("^f-|^b-|-returns.csv","",grep("-returns.csv",list.files(retDir),value=TRUE))
}

if (!onlyretfiles) {
    ## return files were created now create holding, rebaltrades, and rebalDates files from them
    factors <- fnames # c("smom.t","lvol.t","value.t","quality.t","size.t","cwall","neutral")
    nLag <- 0
    counter <- 0

    for (factor in factors) {
    	counter <- counter + 1
        if (exists("factornames") && !is.null(factornames)) {
    	   factorname <- factornames[counter]
        } else factorname <- factor
    	if (exists("tickers") && !is.null(tickers)) {
    		ticker <- tickers[counter]
    	} else ticker <- factorname
        if (factor == "cwall") {
            prefix <- "b-"
        } else prefix <- "f-"
        if (exists("Factors")) {
            fret <- Factors[,c("DATE",factor),with=FALSE]
        } else {
            fret <- fread(file.path(retDir,paste0(prefix,factor,"-returns.csv")))
        }
        fret[,DATE:=as.Date(DATE)]
        friday <- listDay(fret$DATE, dayName='friday', nLag=nLag)
        fDates <- as.Date(friday$firstDayEndQuarter) # formation dates: First friday
        rebalDates <- c(as.Date(fret[,DATE][1])-days(1),fDates)
        fDates <- rebalDates
        write.csv(data.table(date=as.Date(rebalDates)),file=file.path(retDir,'rebalDates.csv'), row.names=FALSE)
        
        holcols <- c("DATE","ID","Weight","sedol","ticker","security_name","price_usd",
        	"iso_country_symbol","iso_currency_symbol","atv","dtt10","sector","fbsector",
        	"div_yield","price_to_sales","price_to_book_value","price_to_cash_earnings",
        	"price_to_earnings","roe")
        
        holdings <- data.table(matrix(ncol = length(holcols), nrow = length(fDates)))
        setnames(holdings,names(holdings),holcols)
        holdings[,DATE:=as.Date(fDates)]
        holdings[,ID:=NULL]; holdings[,ID:=factor]
        holdings[,Weight:=NULL]; holdings[,Weight:=1]
        holdings[,ticker:=NULL]; holdings[,ticker:=ticker]
        holdings[,security_name:=NULL]; holdings[,security_name:=factorname]
        holdings[,iso_country_symbol:=NULL]; holdings[,iso_country_symbol:="US"]
        holdings[,iso_currency_symbol:=NULL]; holdings[,iso_currency_symbol:="USD"]
        holdings <- holdings[,holcols,with=FALSE]
        write.csv(holdings,file=file.path(retDir,paste0(prefix,factor,"-holdings.csv")), row.names=FALSE,na="")
        
        rebaltrades <- data.table(DATE=as.Date(fDates),ID=factor,trade=0)
        write.csv(rebaltrades,file=file.path(retDir,paste0(prefix,factor,"-rebaltrades.csv")), row.names=FALSE)
        if (factor == "cwall" && !is.element("neutral",factors) ) {
          write.csv(holdings,file=file.path(retDir,"f-neutral-holdings.csv"), row.names=FALSE, na="")  
          write.csv(rebaltrades,file=file.path(retDir,"f-neutral-rebaltrades.csv"), row.names=FALSE)
        }
    }
}
message("Done generating the files in ", retDir)
