#!/usr/bin/env Rscript

if (exists("runargs")) {
  ## e.g. run from debugger with
  argList <- strsplit(runargs, ' ')[[1]]
} else {
  argList <- commandArgs(trailingOnly = TRUE)
}

## Take warnings seriously
options(warn=2)

suppressWarnings(library("optparse"))
suppressWarnings(library("data.table"))
suppressWarnings(library("zoo"))

## Process the command line options:
option_list <- list(
  make_option(c("--universe"),default="fbu-developed-core",help="universe name .[default %default]"),
  make_option(c("--factorallocation"),default="~/rails/stagingfa/fbdata/universes/fb_fbu_core_default/users/328/uports/0/u-relQSynthRBFA.csv",
  		help="file with the factor allocation.[default %default]"),
  make_option(c("--holdingsdir"),default="~/rails/stagingfa/fbdata/universes/fb_fbu_core_default",
  		help="directory with the factor holdings files, like f-value.t-holdings.csv [default %default]"),
  make_option(c("--strategy"),default="factorblend",help="strategy name (tag) for the portfolio files that will be created.[default%default]"),
  make_option(c("--outdir"),default="~/work/betablender-cl/Portfolios/fbu-developed-core/nosplit",
  	    help="Portfolio Directory where output files are stored in. By default it is the ~/work/betablender-cl/Portfolios/[universe]/nosplit")
)

cmdArgs <- parse_args(OptionParser(option_list=option_list), argList,positional_arguments=TRUE)
opt <- cmdArgs$options
universe <- tolower(opt$universe)
factorAllocation <- opt$factorallocation
holdingsdir <- opt$holdingsdir
strategy <- opt$strategy
outdir <- opt$outdir

#universe <- "fbu-developed-core"  # fbu-developed-core russell1k-developed-core
#userID <- "1424"
#portfolioID <- "49"
#strategy <- "PHerc" # "ercph" bffr1k 

#holdingsdir <- paste0("~/work/betablender-cl/blends4Monty")
#factorAllocation <- fread(file.path("~/work/betablender-cl/blends4Monty",userID,portfolioID,"u-relQSynthRBFA.csv"))[,list(portfolio,asOf,weight)]

if (is.null(outdir)) {
   outdir <- paste0("~/work/betablender-cl/Portfolios/",universe,"/nosplit")
   dir.create(paste0("~/work/betablender-cl/Portfolios/",universe), showWarnings = FALSE)
   dir.create(outdir, showWarnings = FALSE)
}

factorAllocation[,quarter:=as.yearqtr(asOf, format = "%Y-%m-%d")]
factorfiles <- grep("f-",grep("-holdings",list.files(holdingsdir),value=TRUE),value=TRUE)
holdings <- lapply(file.path(holdingsdir,factorfiles),function(x) { fread(x)[,list(DATE,ID,ticker,Weight,sector,fbsector)]  })
names(holdings) <-  factorfiles
suppressWarnings(names(holdings) <- gsub("f-","",gsub("-holdings.csv","",factorfiles)))

fDates <- unique(holdings[[1]]$DATE)
qDates <- as.yearqtr(fDates, format = "%Y-%m-%d")

Qdates <- intersect(as.character(unique(factorAllocation$quarter)),as.character(qDates))

rep.row <- function(x,n){
   matrix(rep(x,each=n),nrow=n)
}

for (date in fDates) {
	dateQ <- as.yearqtr(date, format = "%Y-%m-%d")
	if (is.element(as.character(dateQ),Qdates)) {
		dateholdings <- lapply(holdings, function(x) { x[DATE==date,list(ID,Weight)]} )
		dateholdings <- lapply(names(dateholdings),function(i) setNames(dateholdings[[i]], c('ID', i)))
		fholdings <- Reduce(f = function(x,y) {merge(x,y, all=TRUE, by = "ID")},dateholdings)
		fholdings[is.na(fholdings)] <- 0
		fa <- factorAllocation[quarter==dateQ,weight]
		# fa <- fa/sum(fa) ## add a check of sum(fa) == 1 here?
		names(fa) <- factorAllocation[quarter==dateQ,portfolio]
		FA <- as.data.table(rep.row(fa,nrow(fholdings)))
		setnames(FA,names(FA),names(fa))
		blendWeights <- data.table(ID=fholdings$ID , Weight=rowSums(fholdings[,names(FA),with=FALSE]*FA),DATE=date)
		weightsFileName <- paste0(strategy,"-",date,".csv")
		write.csv(blendWeights,file=file.path(outdir,weightsFileName),row.names=FALSE)
	}
}

message("Done building the portfolio files for ", strategy, " in ",universe, " no run: ")
message("./BuildDailyTracker.R -s ",universe, " --trackertag=",strategy," --trackerfiles=turnover --strategy=",strategy," --selection=all --verbosity=100")
message("./BuildPositions.R -s ",universe, " --columns=ticker,sector,fbsector,adjmktcap --strategy=",strategy," --mergedates --verbosity=100")

quit(save="no")
#### 

#./BuildDailyTracker.R -s fbu-developed-core --trackertag=ercph --trackerfiles=turnover --strategy=ercph --selection=all
#SNAPDATE='2017-02-28'
#./BuildDailyTracker.R -s fbu-developed-core --trackertag=ercph --strategy=ercph --trackerfiles=weights,turnover,rebaltrades --snapcloses=${SNAPDATE} --verbosity=100

